import random
import csv
import warnings
from pprint import pprint
from datetime import time
import pandas as pd
from scipy.stats import pearsonr
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta

import numpy as np
from nltk.tokenize import sent_tokenize
from sklearn.cross_validation import Bootstrap, LeaveOneOut
from sklearn.feature_extraction import DictVectorizer
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import f1_score, classification_report, precision_recall_fscore_support, accuracy_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import cross_validation
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline, FeatureUnion

from main_issue_affectiveness import fixing_time_politeness_boxplot
from main_magnet_sticky import POLITE_DICT
from politeness.request_utils import check_is_request
from utility.affective_classifier import EmotionClassifier, LemmaTokenizer, create_bow_training_set, \
    AffectiveTransformer, EmotionsClassifier
from utility.dao import PostgreSQLDAO
from utility.datasetUtility import createRandomTestSet, COMMENT_BY_PROJECT_NAME, csvfile2list, \
    ISSUE_OPENED_PER_TIME_INTERVAL, ISSUE_CLOSED_PER_TIME_INTERVAL, AVG_ISSUE_RESOLUTION_TIME_PER_TIME_INTERVAL

__author__ = 'marco'

project_list = ['HBase', 'Hadoop Common', 'Hadoop HDFS', 'Derby',
                'Lucene - Core', 'Hadoop Map/Reduce', 'Cassandra',
                'Hive', 'Solr', 'Harmony', 'OFBiz', 'Infrastructure',
                'Camel', 'ZooKeeper', 'groovy', 'JBoss']

MAGNET_STICKY_RAW_DATA_DIR = './data/politeness-raw/'
MAGNET_STICKY_DATA_DIR = './data/magnet-sticky/'
AFFECTIVENESS_DATA_DIR = './data/affectiveness/'
POLITENESS_DATA_DIR = './data/politeness/'

dao = PostgreSQLDAO("localhost", "jira_dataset", "jira_user", "jira_user")


def tfidf_transformer_example():
    emotion = 'anger'
    training_set, training_set_labels, docs = create_bow_training_set(emotion)
    # Creates a basic pipeline [docs]->[tfidf-vectors]->[svm classifier]
    pipeline = Pipeline([
        ('tfidf_vect', TfidfVectorizer(strip_accents='unicode',
                                       tokenizer=LemmaTokenizer(),
                                       stop_words='english',
                                       max_df=0.1,
                                       decode_error='ignore',
                                       analyzer='word',
                                       norm='l2',
                                       ngram_range=(1, 2)
                                       )),
        ('clf', SVC(probability=True,
                    C=10,
                    kernel='linear')),  # SGDClassifier()),
    ])
    # Converts the corpus of documents into a Tf-idf-weighted document-term matrix
    tfidf_docs_matrix = pipeline.named_steps['tfidf_vect'].fit_transform(training_set)
    # Returns the features extracted in a list of word-terms
    vocabulary = pipeline.named_steps['tfidf_vect'].get_feature_names()
    print 'training set lenght: %d' % len(training_set)
    print 'vocabulary lenght: %d' % len(vocabulary)
    print 'tfidf_docs_matrix shape : %s' % str(tfidf_docs_matrix._shape)
    print 'Document text example:\n\t%s' % training_set[0]
    print 'Document first component index:\n\t%s' % vocabulary[tfidf_docs_matrix[0].indices[0]]
    # Given the tranformed matrix it returns the matrix of word vectors with non zero values
    print 'Document vector:\n\t%s' % pipeline.named_steps['tfidf_vect'].inverse_transform(tfidf_docs_matrix)[0]
    print 'tfidf_docs_matrix dict:'
    print '\t' + "\n\t".join(tfidf_docs_matrix.__dict__.keys())
    print tfidf_docs_matrix.indices
    print tfidf_docs_matrix.data

    print '----------------------------------------------------------------------------\n'
    text = training_set[10]
    X = tfidf_docs_matrix[10]
    transformer = FeatureUnion([
        ('bow', TfidfVectorizer(strip_accents='unicode',
                                tokenizer=LemmaTokenizer(),
                                stop_words='english',
                                # max_df=0.1,
                                decode_error='ignore',
                                analyzer='word',
                                norm='l2',
                                ngram_range=(1, 2)
                                )),
        ('affective', Pipeline([('extract', AffectiveTransformer()),
                                ('vectorize', DictVectorizer())])
         )
    ])
    print X
    print
    print transformer.fit_transform([text])


def grid_search_example():
    training_set, training_set_labels, docs = EmotionClassifier.create_bow_training_set('anger')
    parameters = {
        'vect__max_df': [0.25, 0.5, 0.75, 1.0],
        'vect__max_features': [None, 5000, 10000, 50000],
        'vect__ngram_range': [[1, 2]],  # unigrams or bigrams
        'tfidf__norm': ['l2', 'l1'],
        'clf__kernel': ['linear', 'rbf'],
        'clf__C': [0.1, 1, 10, 100]
    }
    pipeline = Pipeline([
        ('tfidf_vect', TfidfVectorizer(strip_accents='unicode',
                                       tokenizer=LemmaTokenizer(),
                                       stop_words='english',
                                       max_df=0.1,
                                       decode_error='ignore',
                                       analyzer='word',
                                       norm='l2',
                                       ngram_range=(1, 2))),
        ('clf', SVC(probability=True,
                    C=10,
                    shrinking=True,
                    kernel='linear')),  # SGDClassifier()),
    ])
    grid_search = GridSearchCV(pipeline, parameters, n_jobs=-1, verbose=1, score_func=f1_score)

    print("Performing grid search...")
    print("pipeline:", [name for name, _ in pipeline.steps])
    print("parameters:")
    pprint(parameters)
    t0 = time()
    grid_search.fit(training_set, training_set_labels)
    print("done in %0.3fs" % (time() - t0))
    print()

    print("Best score: %0.3f" % grid_search.best_score_)
    print("Best parameters set:")
    best_parameters = grid_search.best_estimator_.get_params()
    for param_name in sorted(parameters.keys()):
        print("\t%s: %r" % (param_name, best_parameters[param_name]))


def mlc_bootstrap(emotion, n_iter=100):
    print "creating emotion classifier..."
    emotion_mlc = EmotionClassifier(emotion)
    print "classifier created."
    pipeline = emotion_mlc.pipeline
    print 'Classification report: \n'
    x_train, x_test, y_train, y_test = cross_validation.train_test_split(emotion_mlc.x_train,
                                                                         emotion_mlc.y_train,
                                                                         test_size=.1,
                                                                         random_state=random.randint(1, 100))
    pipeline.fit(x_train, y_train)
    predicted = pipeline.predict(x_test)
    print classification_report(y_test, predicted, target_names=['non-' + emotion, emotion])
    n = len(emotion_mlc.x_train)
    print 'Bootstrap with %d iterations:\n' % n_iter
    bs = Bootstrap(n,
                   train_size=int(0.9 * n),
                   test_size=int(0.1 * n),
                   n_iter=n_iter)
    results = []
    for train, test in bs:
        xtrain, ytrain, xtest, ytest = [emotion_mlc.x_train[i] for i in train], [emotion_mlc.y_train[i] for i in train], \
                                       [emotion_mlc.x_train[i] for i in test], [emotion_mlc.y_train[i] for i in test]
        pipeline.fit(xtrain, ytrain)
        predicted = pipeline.predict(xtest)
        results.append(tuple([accuracy_score(ytest, predicted)]) + precision_recall_fscore_support(ytest, predicted))
    print '\taccuracy\tprecision\trecall\tf1-score\ts\n'
    print '\t%.3f    \t%.3f\t    %.3f\t%.3f\t   %.3f' % tuple(map(np.mean, zip(*results)))


def mlc_leave_one_out(emotion):
    emotion_mlc = EmotionClassifier(emotion)
    pipeline = emotion_mlc.pipeline
    n_iter = len(emotion_mlc.x_train)
    print 'Leave one out with %d iterations:\n' % n_iter
    loo = LeaveOneOut(n_iter)
    results = []
    i = 0
    for train, test in loo:
        xtrain, ytrain, xtest, ytest = [emotion_mlc.x_train[i] for i in train], [emotion_mlc.y_train[i] for i in train], \
                                       [emotion_mlc.x_train[test]], [emotion_mlc.y_train[test]]
        pipeline.fit(xtrain, ytrain)
        predicted = pipeline.predict(xtest)
        results.append([accuracy_score(ytest, predicted)] + map(lambda x: x[0], precision_recall_fscore_support(ytest,
                                                                                                                pipeline.predict(
                                                                                                                    xtest))))
    print '\taccuracy\tprecision\trecall\tf1-score\ts\n'
    print '\t%.3f    \t%.3f\t    %.3f\t%.3f\t   %.3f' % tuple(map(np.mean, zip(*results)))


def create_emotion_features_csv(emotion):
    # get the training set
    affective_feature_matrix = []
    print "Creating training documents...."
    x_train, y_train, docs = create_bow_training_set(emotion)
    print "trainig documents created."
    print "Extracting affective features..."
    for i in range(0, len(docs)):
        affective_features = AffectiveTransformer.extract_affective_features(x_train[i])
        affective_features.insert(0, docs[i].name)
        affective_features.insert(1, emotion if y_train[i] == 1 else 'non-' + emotion)
        affective_features.insert(len(affective_features), x_train[i])
        affective_feature_matrix.append(affective_features)
    print "Affective features created."
    print "Writing results..."
    with open("data/anger_training_affective_features_.csv", "wb") as csvfile:
        writer = csv.writer(csvfile,
                            delimiter=',',
                            quotechar='"')
        writer.writerow(['name', 'emotion', 'mood', 'modality', 'sentiment_score', 'polite_score',
                         'impolite_score', 'comment'])
        for t in affective_feature_matrix:
            writer.writerow(t)
    print "Finished."


def emotionColumnIndex(emotion):
    dict = {"love": 1, "joy": 2, "surprise": 3, "anger": 4, "sadness": 5, "fear": 6, "neutral": 7, "comment N": 9}
    return dict[emotion]


def test_classifier(emotion):
    emotion_mlc = EmotionClassifier(emotion)
    docs_ids = [doc.name for doc in emotion_mlc.docs]
    test_docs = createRandomTestSet(docForSentence=True, training_ids=docs_ids, size=10000)
    print "Classifing test set..."
    with open("data/test_set_%s.csv" % emotion.upper(), "wb") as csvfile:
        writer = csv.writer(csvfile,
                            delimiter=',',
                            quotechar='"')
        writer.writerow(
            ["id", "love", "joy", "surprise", "anger", "sadness", "fear", "neutral", "probability", "comment"])
        for doc in test_docs:
            emotion_dict = emotion_mlc.predict(doc.description, probability=True)
            emotion_label = emotion_mlc.predict(doc.description)
            if (emotion == emotion_label):
                row = [""] * 10
                row[0] = doc.name
                row[emotionColumnIndex(emotion)] = "x"
                row[8] = emotion_dict[emotion]
                row[9] = doc.description
                writer.writerow(row)
    print "Finished."


def create_project_affectiveness_metrics(project_name, emotion_threshold=None):
    unicode_error = 0
    print 'writing project %s...%d/%d' % (project_name, project_list.index(project_name), len(project_list))
    project_file_name = project_name.lower().replace("/", "-").replace(" ", "-")
    csv_file_name = "%s%s.csv" % (AFFECTIVENESS_DATA_DIR, project_file_name)
    affect_file_name = "%s%s.csv" % (MAGNET_STICKY_RAW_DATA_DIR, project_file_name)
    affective_metrics = csvfile2list(affect_file_name)
    comments = dao.executeQuery(COMMENT_BY_PROJECT_NAME % project_name)
    print "number of comments: %d " % len(comments)
    affect_metric_len = len(affective_metrics)
    print "number of affective metrics: %d " % affect_metric_len
    mlc = EmotionsClassifier() if emotion_threshold == None else EmotionsClassifier(emotion_threshold)
    with open(csv_file_name, 'wb') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE)
        writer.writerow(['user_id', 'comment_id', 'comment_date', 'mood', 'modality', 'sentiment',
                         'polite_label', 'polite_confidence', 'anger', 'sadness', 'joy', 'love', 'num_sentences'])
        i = 0
        for affective_metric in affective_metrics:
            comment_row = filter(lambda x: str(x['id']) == str(affective_metric['comment_id']), comments)
            if (comment_row):
                try:
                    comment_row = comment_row[0]
                    emotions = mlc.predict(comment_row['comment'])
                    num_sentences = len(sent_tokenize(comment_row['comment']))
                    row = [affective_metric['user_id'],
                           affective_metric['comment_id'],
                           affective_metric['comment_date'],
                           affective_metric['mood'],
                           affective_metric['modality'],
                           affective_metric['sentiment'],
                           affective_metric['polite_label'],
                           affective_metric['polite_confidence'],
                           emotions['anger'],
                           emotions['sadness'],
                           emotions['joy'],
                           emotions['love'],
                           num_sentences]
                    writer.writerow(row)
                except UnicodeDecodeError:
                    unicode_error += 1
                except UnicodeEncodeError:
                    unicode_error += 1
            i += 1
            if (i % 1000) == 0:
                print "processed %.2f%% comments" % (float(i) / affect_metric_len * 100)
    print "Unicode errors : %d" % unicode_error


def affectiveness_over_time(project_name):
    print "analyzing project: %s" % project_name
    project_file_name = project_name.lower().replace("/", "-").replace(" ", "-")
    affective_metrics = csvfile2list("%s%s.csv" % (AFFECTIVENESS_DATA_DIR, project_file_name))
    min_date = min([parse(x['comment_date']) for x in affective_metrics])
    max_date = max([parse(x['comment_date']) for x in affective_metrics])
    print 'date\tsentiment\tpolite\tanger\tsadness\tjoy\tlove\tneutral' \
          '\tmagnet\tsticky\topened_issues\tclosed_issues\tavg_fixing_time'
    for curr_start_date in pd.date_range(min_date, max_date, freq='m'):
        prev_start_date = curr_start_date + relativedelta(months=-1)
        curr_end_date = curr_start_date + relativedelta(months=+1)
        next_end_date = curr_start_date + relativedelta(months=+2)
        metrics_per_week = filter(lambda x: parse(x['comment_date']) <= curr_end_date
                                            and parse(x['comment_date']) >= curr_start_date,
                                  affective_metrics)
        num_sentences = sum([int(x['num_sentences']) for x in metrics_per_week])
        if num_sentences > 0:
            try:
                m = [np.mean([float(x['sentiment']) for x in metrics_per_week]),
                     np.mean([POLITE_DICT[x['polite_label']] for x in metrics_per_week
                              if abs(float(x['polite_confidence'])) > 0.7]),
                     sum([int(x['anger']) for x in metrics_per_week]) / float(num_sentences) * 100,
                     sum([int(x['sadness']) for x in metrics_per_week]) / float(num_sentences) * 100,
                     sum([int(x['joy']) for x in metrics_per_week]) / float(num_sentences) * 100,
                     sum([int(x['love']) for x in metrics_per_week]) / float(num_sentences) * 100,
                     sum([int(x['num_sentences']) - int(x['love']) - int(x['sadness']) - int(x['joy']) - int(x['anger'])
                          for x in metrics_per_week]) / float(num_sentences) * 100]
                dev_prev_observed_time = set([x['user_id'] for x in affective_metrics
                                              if parse(x['comment_date']) >= prev_start_date
                                              and parse(x['comment_date']) <= curr_start_date])
                dev_curr_observed_time = set([x['user_id'] for x in metrics_per_week])
                dev_next_observed_time = set([x['user_id'] for x in affective_metrics
                                              if parse(x['comment_date']) >= curr_end_date
                                              and parse(x['comment_date']) <= next_end_date])
                num_dev_sticky = len(dev_curr_observed_time)
                num_dev_magnet = len(dev_prev_observed_time) + num_dev_sticky
                sticky = float(len(dev_next_observed_time & dev_curr_observed_time)) / num_dev_sticky * 100 \
                    if num_dev_sticky else 0.0
                magnet = float(len(dev_curr_observed_time - dev_prev_observed_time)) / num_dev_magnet * 100 \
                    if num_dev_magnet else 0.0
                opened_issues = dao.executeQuery(ISSUE_OPENED_PER_TIME_INTERVAL,
                                                 params={'project_name': project_name,
                                                         'curr_date': curr_start_date,
                                                         'next_date': curr_end_date})[0][0]
                closed_issues = dao.executeQuery(ISSUE_CLOSED_PER_TIME_INTERVAL,
                                                 params={'project_name': project_name,
                                                         'curr_date': curr_start_date,
                                                         'next_date': curr_end_date})[0][0]
                avg_fixing_time = dao.executeQuery(AVG_ISSUE_RESOLUTION_TIME_PER_TIME_INTERVAL,
                                                   params={'project_name': project_name,
                                                           'curr_date': curr_start_date,
                                                           'next_date': curr_end_date})[0][0]
                avg_fixing_time = 0.0 if avg_fixing_time == None else avg_fixing_time

                print "%s\t%0.3f\t%.3f\t%.3f\t%.3f\t%0.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%d\t%d\t%.3f" \
                      % (curr_start_date.strftime('%Y-%m'), m[0], m[1], m[2], m[3], m[4], m[5], m[6],
                         magnet, sticky, opened_issues, closed_issues, avg_fixing_time)
            except KeyError:
                pass


def evaluate_politeness_for_requests(project_name):
    project_file_name = project_name.lower().replace("/", "-").replace(" ", "-")
    affective_metrics = csvfile2list("%s%s.csv" % (AFFECTIVENESS_DATA_DIR, project_file_name))
    print "Number of comments %d" % len(affective_metrics)
    affective_metrics = [x for x in affective_metrics if int(x['num_sentences']) == 2]
    num_of_requests = len(affective_metrics)
    print "Number of comments with 2 sentences %d" % num_of_requests
    requests = []
    csv_file_name = "%s%s.csv" % (POLITENESS_DATA_DIR, project_file_name)
    i = 0
    with open(csv_file_name, 'wb') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE)
        header = ['user_id', 'comment_id', 'comment_date', 'mood', 'modality', 'sentiment', 'polite_label',
                  'polite_confidence', 'anger', 'sadness', 'joy', 'love', 'num_sentences']
        # writer.writerow(header)
        for affective_row in affective_metrics:
            if float(affective_row['polite_confidence']) >= 0.8:
                comment = dao.findById('jira_issue_comment', int(affective_row['comment_id']))[0]
                try:
                    doc = AffectiveTransformer.stanford_dependency_parser(comment['comment'])
                    is_request = check_is_request(doc)
                except TypeError:
                    continue
                if is_request and "would" in comment['comment']:
                    # writer.writerow([affective_row[k] for k in header])
                    print affective_row['comment_id'], \
                        affective_row['polite_label'], \
                        affective_row['polite_confidence'], \
                        comment['comment']
                    print "-" * 50
                    # console_progress_bar(i, num_of_requests)
                    # print "Number of request comments with 2 sentences %d" % len(requests)


def create_magnet_sticky_data(project_name):
    print "analyzing project: %s" % project_name
    project_file_name = project_name.lower().replace("/", "-").replace(" ", "-")
    affective_metrics = csvfile2list("%s%s.csv" % (AFFECTIVENESS_DATA_DIR, project_file_name))
    min_date = min([parse(x['comment_date']) for x in affective_metrics])
    max_date = max([parse(x['comment_date']) for x in affective_metrics])
    with open("%s%s.csv" % (MAGNET_STICKY_DATA_DIR, project_file_name), 'wb') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE)
        header = ['date', 'magnet', 'sticky', 'avg_politeness']
        writer.writerow(header)
        for curr_start_date in pd.date_range(min_date, max_date, freq='m'):
            prev_start_date = curr_start_date + relativedelta(months=-1)
            curr_end_date = curr_start_date + relativedelta(months=+1)
            next_end_date = curr_start_date + relativedelta(months=+2)
            metrics_per_week = filter(lambda x: parse(x['comment_date']) <= curr_end_date
                                                and parse(x['comment_date']) >= curr_start_date,
                                      affective_metrics)
            dev_prev_observed_time = set([x['user_id'] for x in affective_metrics
                                          if parse(x['comment_date']) >= prev_start_date
                                          and parse(x['comment_date']) <= curr_start_date])
            dev_curr_observed_time = set([x['user_id'] for x in metrics_per_week])
            dev_next_observed_time = set([x['user_id'] for x in affective_metrics
                                          if parse(x['comment_date']) >= curr_end_date
                                          and parse(x['comment_date']) <= next_end_date])
            num_dev_sticky = len(dev_curr_observed_time)
            num_dev_magnet = len(dev_prev_observed_time) + num_dev_sticky

            sticky = float(len(dev_next_observed_time & dev_curr_observed_time)) / num_dev_sticky * 100 \
                if num_dev_sticky else 0.0
            magnet = float(len(dev_curr_observed_time - dev_prev_observed_time)) / num_dev_magnet * 100 \
                if num_dev_magnet else 0.0
            avg_politeness = np.mean([POLITE_DICT[x['polite_label']] for x in metrics_per_week
                                      if abs(float(x['polite_confidence'])) > 0.7])
            avg_politeness = avg_politeness if avg_politeness != np.nan else 0.0
            writer.writerow([curr_end_date,
                             "%0.4f" % magnet,
                             "%0.4f" % sticky,
                             "%0.4f" % avg_politeness])


def politeness_over_time(project_name):
    print "analyzing project: %s" % project_name
    project_file_name = project_name.lower().replace("/", "-").replace(" ", "-")
    affective_metrics = csvfile2list("%s%s.csv" % (AFFECTIVENESS_DATA_DIR, project_file_name))
    min_date = min([parse(x['comment_date']) for x in affective_metrics])
    max_date = max([parse(x['comment_date']) for x in affective_metrics])
    print 'date\tpolite\timpolite\tavg politeness\tmagnet\tsticky'
    avg_pol_list, pol_list, mag_list, stic_list = [], [], [], []
    for curr_start_date in pd.date_range(min_date, max_date, freq='12m'):
        prev_start_date = curr_start_date + relativedelta(months=-12)
        curr_end_date = curr_start_date + relativedelta(months=+12)
        next_end_date = curr_start_date + relativedelta(months=+24)
        metrics_per_week = filter(lambda x: parse(x['comment_date']) <= curr_end_date
                                            and parse(x['comment_date']) >= curr_start_date,
                                  affective_metrics)
        num_sentences = sum([int(x['num_sentences']) for x in metrics_per_week])

        if num_sentences > 0:
            try:
                polite = [POLITE_DICT[x['polite_label']] for x in metrics_per_week
                          if abs(float(x['polite_confidence'])) > 0.7
                          and x['polite_label'] == 'polite']

                impolite = [-POLITE_DICT[x['polite_label']] for x in metrics_per_week
                            if abs(float(x['polite_confidence'])) > 0.7
                            and x['polite_label'] == 'impolite']
                avg_politeness = np.mean([POLITE_DICT[x['polite_label']] for x in metrics_per_week
                                          if abs(float(x['polite_confidence'])) > 0.7])
                len_pol_com = len(polite) + len(impolite)
                m = [100 * float(sum(polite)) / len_pol_com,
                     100 * float(sum(impolite)) / len_pol_com,
                     avg_politeness]
                dev_prev_observed_time = set([x['user_id'] for x in affective_metrics
                                              if parse(x['comment_date']) >= prev_start_date
                                              and parse(x['comment_date']) <= curr_start_date])
                dev_curr_observed_time = set([x['user_id'] for x in metrics_per_week])
                dev_next_observed_time = set([x['user_id'] for x in affective_metrics
                                              if parse(x['comment_date']) >= curr_end_date
                                              and parse(x['comment_date']) <= next_end_date])
                num_dev_sticky = len(dev_curr_observed_time)
                num_dev_magnet = len(dev_prev_observed_time) + num_dev_sticky
                sticky = float(len(dev_next_observed_time & dev_curr_observed_time)) / num_dev_sticky * 100 \
                    if num_dev_sticky else 0.0
                magnet = float(len(dev_curr_observed_time - dev_prev_observed_time)) / num_dev_magnet * 100 \
                    if num_dev_magnet else 0.0
                print "%s-%s\t%.3f\t%0.3f\t%.3f\t%.3f\t%.3f" % (curr_start_date.strftime('%Y'),
                                                                curr_end_date.strftime('%Y'),
                                                                m[0], m[1], m[2], magnet, sticky)
                pol_list.append(m[0])
                avg_pol_list.append(m[2])
                mag_list.append(magnet)
                stic_list.append(sticky)
            except KeyError:
                pass
    pol_list = pol_list[:-1]
    mag_list = mag_list[:-1]
    stic_list = stic_list[:-1]
    avg_pol_list = avg_pol_list[:-1]
    print "Magnet vs Politeness\tSticky vs Politeness"
    print "%.4f\t%.4f" % (pearsonr(np.array(pol_list), np.array(mag_list))[0],
                          pearsonr(np.array(pol_list), np.array(stic_list))[0])

    print "Magnet vs AVG Politeness\tSticky vs AVG Politeness"
    print "%.4f\t%.4f" % (pearsonr(np.array(avg_pol_list), np.array(mag_list))[0],
                          pearsonr(np.array(avg_pol_list), np.array(stic_list))[0])

    pol_list = (pol_list - np.mean(pol_list)) / (np.std(pol_list) * len(pol_list))
    mag_list = (mag_list - np.mean(mag_list)) / np.std(mag_list)
    stic_list = (stic_list - np.mean(stic_list)) / np.std(stic_list)

    print "Cross Correlation"
    print "Magnet vs Politeness Std, Max, Min, Lag 1, Lag 2, Lag 3"
    pol_mag = np.correlate(pol_list, mag_list, 'full')
    print "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t" % (
        max(pol_mag), min(pol_mag), np.correlate(pol_list, mag_list), pol_mag[0], pol_mag[1], pol_mag[2])
    print "Sticky vs Politeness Std, Max, Min, Lag 1, Lag 2, Lag 3"
    pol_stic = np.correlate(pol_list, stic_list, 'full')
    print "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t" % (
        max(pol_stic), min(pol_stic), np.correlate(pol_list, stic_list)[0], pol_stic[0], pol_stic[1], pol_stic[2])


def create_project_raw_affectiveness_metrics(project_name):
    print 'writing project %s' % project_name
    project_file_name = project_name.lower().replace("/", "-").replace(" ", "-")
    csv_file_name = "%s%s.csv" % (AFFECTIVENESS_DATA_DIR, project_file_name)
    comments = dao.executeQuery(COMMENT_BY_PROJECT_NAME % project_name)
    print "number of comments: %d " % len(comments)
    mlc = EmotionsClassifier()
    with open(csv_file_name, 'wb') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE)
        writer.writerow(['user_id', 'comment_id', 'comment_date', 'mood', 'modality', 'sentiment',
                         'polite_label', 'polite_confidence', 'anger', 'sadness', 'joy', 'love', 'num_sentences'])
        i = 1
        for comment in comments:
            if (i % 1000) == 0:
                print "analysing comment %d..." % i
            i += 1
            try:
                emotions = mlc.predict(comment['comment'])
                num_sentences = len(sent_tokenize(comment['comment']))
                affect_features = AffectiveTransformer.affective_features(comment['comment'])
                row = [comment['author_id'],
                       comment['id'],
                       comment['creationdate'],
                       affect_features['mood'],
                       affect_features['modality'],
                       affect_features['sentiment'],
                       affect_features['polite_label'],
                       affect_features['polite_confidence'],
                       emotions['anger'],
                       emotions['sadness'],
                       emotions['joy'],
                       emotions['love'],
                       num_sentences]
                writer.writerow(row)

            except UnicodeDecodeError:
                pass


if __name__ == '__main__':
    warnings.filterwarnings('ignore')
    # emotion = 'love'
    # args = sys.argv
    # project_name = args[1]
    # create_project_affectiveness_metrics('ZooKeeper', emotion_threshold=0.9)

    # mlc_bootstrap(emotion, n_iter=1000)
    # test_classifier(emotion)

    # Creates test documents
    # docs_ids = [doc.name
    # for doc in create_docs_from_msr_dataset(emotion='anger',
    # filterEmotion=True,
    # docForSentence=True)]

    # print len(test_docs)

    # create_emotion_features_csv(emotion)
    # mlc_leave_one_out(emotion)

    # politeness_over_time('Infrastructure')

    project_name = "hibernate-orm"

    # fixing_time_politeness_boxplot(project_name)

    # politeness_over_time(project_name)
    #for project_name in project_list:
    # create_project_raw_affectiveness_metrics(project_name)

    # evaluate_politeness_for_requests(project_name)

    # create_magnet_sticky_data('Infrastructure')

    # project_name = 'ZooKeeper'
    #
    # project_file_name = MAGNET_STICKY_DATA_DIR+project_name.lower().replace("/", "-").replace(" ", "-")+'.csv'
    # magnet_metrics = csvfile2list(project_file_name)
    #
    # politeness = [float(x['avg_politeness']) for x in magnet_metrics]
    # magnet = [float(x['magnet']) for x in magnet_metrics]
    # sticky = [float(x['sticky']) for x in magnet_metrics]

    # politeness = (politeness - np.mean(politeness)) / (np.std(politeness) * len(politeness))
    # magnet = (magnet - np.mean(magnet)) /  np.std(magnet)
    # sticky = (sticky - np.mean(sticky)) /  np.std(sticky)
    #
    # pol_mag = np.correlate(politeness, magnet, 'full')
    # print max(pol_mag),min(pol_mag)
    # pol_stic = np.correlate(politeness, sticky, 'full')
    # print max(pol_stic),min(pol_stic)
