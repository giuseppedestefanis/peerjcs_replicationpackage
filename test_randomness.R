#this script has been used to calculate the randomness of the politeness time series

magnet_stycky_data <- read.csv("./magnet-sticky/hbase.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2007,03),frequency=12)
plot(gnp)

cox.stuart.test(gnp)
bartels.rank.test(gnp, alternative = "two.sided")
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/hadoop-common.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2006,01),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/derby.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2004,10),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/lucenecore.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2001,11),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)


magnet_stycky_data <- read.csv("./magnet-sticky/hadoop-hdfs.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2006,11),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/cassandra.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2009,06),frequency=12)
plot(stl(gnp,"periodic"))
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/solr.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2006,02),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)


magnet_stycky_data <- read.csv("./magnet-sticky/hive.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2008,10),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/hadoop-map-reduce.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2006,04),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/harmony.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2005,09),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)



magnet_stycky_data <- read.csv("./magnet-sticky/ofbiz.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2006,06),frequency=12)
plot(stl(gnp,"periodic"))
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/infrastructure.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2004,03),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/camel.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2007,06),frequency=12)
plot(stl(gnp,"periodic"),main="Politeness time series - Camel")
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/zookeeper.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2008,07),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)


magnet_stycky_data <- read.csv("./magnet-sticky/geoserver.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2004,03),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/geronimo.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2004,05),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/groovy.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2004,02),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/hibernate-orm.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2004,08),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)


magnet_stycky_data <- read.csv("./magnet-sticky/jboss.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2007,04),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)

magnet_stycky_data <- read.csv("./magnet-sticky/jruby.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2007,07),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)


magnet_stycky_data <- read.csv("./magnet-sticky/pig.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2007,11),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)


magnet_stycky_data <- read.csv("./magnet-sticky/wicket.csv")
gnp <- ts(magnet_stycky_data$avg_politeness,start=c(2006,11),frequency=12)
cox.stuart.test(gnp)
bartels.rank.test(gnp)
difference.sign.test(gnp)


