from nltk import PunktWordTokenizer, PunktSentenceTokenizer
from stanford_parser.parser import Parser
from utility.affective_classifier import affective_features
from utility.dao import PostgreSQLDAO
from utility.datasetUtility import COMMENTS_BY_AUTHOR_ID

__author__ = 'marco'

import unittest

dao = PostgreSQLDAO("localhost", "hong_kong_dataset", "jira_user", "jira_user")


class TestCase(unittest.TestCase):
    def test_politeness_classifier(self):
        rows = dao.executeQuery(COMMENTS_BY_AUTHOR_ID % (759, 'HBase'))
        for r in rows:
            print affective_features(r['comment'])


if __name__ == '__main__':
    unittest.main()
